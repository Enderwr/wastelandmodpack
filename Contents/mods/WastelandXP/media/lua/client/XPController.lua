---
--- XPController.lua
--- Manages XP gains on Wasteland RP server
--- By Zoomies
--- 10/10/2022
---

function showDebugForXp()
    if(not checkedXpDebug) then -- Check if QA team tester
        steamId = getCurrentUserSteamID()
        if(steamId == "76561198304414515" or -- Zoomies
           steamId == "76561198970466789" or -- Bly
           steamId == "76561198362897280" or -- Kuni
           steamId == "76561199443377698" -- Nova
        ) then
            isXpDebugOn = true
        end
        checkedXpDebug = true
    end
    return isXpDebugOn
end


---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@param xp Float
function giveBonusAimAndReloadXp(character, perk, xp)
    if(perk:getId() == "Aiming" or perk:getId() == "Reloading") then
        local extraXP = xp * 0.5 -- Adds 50% more, bringing us to 0.75 if server is set to 0.5 XP gain
        local baseXP = extraXP + xp
        local bonusXP = 0
        local perkLevel = character:getPerkLevel(perk)
        local perkBoost = character:getXp():getPerkBoost(perk)
        if(perk:getId() == "Aiming" and perkLevel > 4 and perkBoost ~= 0) then
            bonusXP = (0.21 * perkBoost) * (baseXP / 0.37)
        end

        if(perk:getId() == "Reloading" and perkLevel > 4 and perkBoost ~= 0) then
            bonusXP = (0.25 * perkBoost) * (baseXP / 0.25)
        end

        character:getXp():AddXP(perk, bonusXP + extraXP, false, false, false)
        if showDebugForXp() then
            local gainedXp = string.format(" +%.2f", tostring(baseXP + bonusXP))
            HaloTextHelper.addTextWithArrow(character, perk:getName() .. " " .. gainedXp, true,
                    HaloTextHelper.getColorGreen())
        end
    end
end

--- Lookup for skills that get bonus XP. If 1.0 is in here, we double the skill gain. If it is 0.5, we are adding 50%
--- If a skill is not in here at all, we don't give any bonus XP, meaning the total XP gain remains at normal
--- For instance, global XP is set to 0.5, and the bonus XP here is 0.5, the total skill xp is 0.75 (75%) of normal
function getBonusXpValues()
    if(not bonusXpValues) then
        bonusXpValues = {}
        bonusXpValues["Cooking"] = 1.0
        bonusXpValues["Farming"] = 1.0
        bonusXpValues["Doctor"] = 1.0
        bonusXpValues["Electricity"] = 1.0
        bonusXpValues["MetalWelding"] = 1.0
        bonusXpValues["Mechanics"] = 1.0
        bonusXpValues["Fishing"] = 0.5
        bonusXpValues["Trapping"] = 0.5
        bonusXpValues["Axe"] = 0.5
        bonusXpValues["Blunt"] = 0.5
        bonusXpValues["SmallBlunt"] = 0.5
        bonusXpValues["LongBlade"] = 0.5
        bonusXpValues["SmallBlade"] = 0.5
        bonusXpValues["Spear"] = 0.5
        bonusXpValues["Maintenance"] = 0.5
        bonusXpValues["Sprinting"] = 0.5
        bonusXpValues["Lightfooted"] = 0.5
        bonusXpValues["Nimble"] = 0.5
        bonusXpValues["Sneaking"] = 0.5
        bonusXpValues["PseudonymousEdPiano"] = 1.0
    end
    return bonusXpValues
end

---@return table
function getCappedPerks()
    if(not cappedPerks) then
        cappedPerks = {}
        cappedPerks["Aiming"] = true
        cappedPerks["Reloading"] = true
        cappedPerks["Woodwork"] = true
        cappedPerks["Cooking"] = true
        cappedPerks["Farming"] = true
        cappedPerks["Doctor"] = true
        cappedPerks["Electricity"] = true
        cappedPerks["MetalWelding"] = true
        cappedPerks["Mechanics"] = true
        cappedPerks["Tailoring"] = true
        cappedPerks["Fishing"] = true
        cappedPerks["Trapping"] = true
        cappedPerks["PlantScavenging"] = true
    end
    return cappedPerks
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@return number of the cap or nil if no cap
function getLevelCap(character, perk)
    local cappedPerks = getCappedPerks()
    if(cappedPerks[perk:getId()])
    then
        local perkBoost = character:getXp():getPerkBoost(perk)
        local cap = 5 + (2 * math.min(3, perkBoost))

        -- Horrible hack for Aim +0 and Reload +0
        if(perk:getId() == "Aiming" or perk:getId() == "Reloading") then
            if(perkBoost == 0) then
                return 6
            end
        end
        return cap
    end
    return nil
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@return boolean true if capped or false otherwise
function doLevelCapping(character, perk)
    local cap = getLevelCap(character, perk)
    if(cap) then
        local perkLevel = character:getPerkLevel(perk)
        if(perkLevel >= cap) then  -- If we have hit the cap, set XP to the cap level exactly
            character:getXp():setXPToLevel(perk, cap)
            return true
        end
    end
    return false
end

function giveBonusXP(character, perk, xp)
    local totalXP = xp
    local bonusXpTable = getBonusXpValues()
    local bonusMultiplier = bonusXpTable[perk:getId()]
    if(bonusMultiplier) then
        local bonusXP = xp * bonusMultiplier
        character:getXp():AddXP(perk, bonusXP, false, false, false)
        totalXP = xp + bonusXP
    end

    if showDebugForXp() and not (perk:getId() == "Sprinting") and not (perk:getId() == "Nimble")
            and not (perk:getId() == "Aiming") and not (perk:getId() == "Reloading") then
        local gainedXp = string.format(" +%.2f", tostring(totalXP))
        HaloTextHelper.addTextWithArrow(character, perk:getName() .. " " .. gainedXp, true,
                HaloTextHelper.getColorGreen())
    end
end

---@param character IsoGameCharacter
---@param perk PerkFactory.Perk
---@param xp Float
function onXP(character, perk, xp)

    if(perk:getId() == "Strength" or perk:getId() == "Fitness") then
        return -- We never do anything with these skills and they spam too much
    end

    local capped = doLevelCapping(character, perk)

    if(not capped) then
        giveBonusAimAndReloadXp(character, perk, xp)
        giveBonusXP(character, perk, xp)
    end
end

Events.AddXP.Add(onXP)