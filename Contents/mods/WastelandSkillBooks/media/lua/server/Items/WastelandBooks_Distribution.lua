require 'Items/ProceduralDistributions'

-- Strength
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandStrength1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandStrength1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandStrength1");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandStrength1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandStrength1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandStrength2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandStrength2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandStrength2");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandStrength2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandStrength2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandStrength3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandStrength3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandStrength3");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandStrength3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandStrength3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 4);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandStrength4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandStrength4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandStrength4");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandStrength4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandStrength4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandStrength5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandStrength5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandStrength5");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandStrength5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandStrength1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 1);

-- Fitness

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandFitness1");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandFitness1");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandFitness1");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandFitness1");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandFitness1");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandFitness2");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandFitness2");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandFitness2");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.7);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandFitness2");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandFitness2");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandFitness3");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandFitness3");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.3);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandFitness3");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.5);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandFitness3");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 4);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandFitness3");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 4);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandFitness4");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandFitness4");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandFitness4");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandFitness4");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 2);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandFitness4");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 2);

table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, "WastelandBooks.WastelandFitness5");
table.insert(ProceduralDistributions["list"]["BookstoreBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, "WastelandBooks.WastelandFitness5");
table.insert(ProceduralDistributions["list"]["LivingRoomShelf"].items, 0.2);
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, "WastelandBooks.WastelandFitness5");
table.insert(ProceduralDistributions["list"]["CrateBooks"].items, 0.3);
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, "WastelandBooks.WastelandFitness5");
table.insert(ProceduralDistributions["list"]["LibraryBooks"].items, 1);
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, "WastelandBooks.WastelandFitness5");
table.insert(ProceduralDistributions["list"]["PostOfficeBooks"].items, 1);