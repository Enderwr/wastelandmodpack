
SkillBook["Fitness"] = {};
SkillBook["Fitness"].perk = Perks.Fitness;
SkillBook["Fitness"].maxMultiplier1 = 2;
SkillBook["Fitness"].maxMultiplier2 = 2;
SkillBook["Fitness"].maxMultiplier3 = 2;
SkillBook["Fitness"].maxMultiplier4 = 4;
SkillBook["Fitness"].maxMultiplier5 = 5;

SkillBook["Strength"] = {};
SkillBook["Strength"].perk = Perks.Strength;
SkillBook["Strength"].maxMultiplier1 = 2;
SkillBook["Strength"].maxMultiplier2 = 2;
SkillBook["Strength"].maxMultiplier3 = 2;
SkillBook["Strength"].maxMultiplier4 = 4;
SkillBook["Strength"].maxMultiplier5 = 5;
