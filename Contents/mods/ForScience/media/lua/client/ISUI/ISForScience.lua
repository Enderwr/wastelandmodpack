ISForScience = {};

local function predicateNotBroken(item)
	return not item:isBroken()
end

ISForScience.bandageContextMenu = function(player, context, items)
	local playerObj = getSpecificPlayer(player)
	local maxLevel = 4
	if(playerObj:getXp():getPerkBoost(Perks.Doctor) == 0) then
		maxLevel = 3
	end
	if(playerObj:getPerkLevel(Perks.Doctor) >= maxLevel) then
		return
	end

	items = ISInventoryPane.getActualItems(items)
	for _, item in ipairs(items) do
		if ((item:isCanBandage() == true) and (item:getBandagePower() >= 2.5)) then
			context:addOption(getText("ContextMenu_FSBandage"), player, ISForScience.onBandage, item);
			break;
		end
	end
end
	
ISForScience.onBandage = function(player, bandage)
	local playerObj = getSpecificPlayer(player);
	local playerInv = playerObj:getInventory();
	ISInventoryPaneContextMenu.transferIfNeeded(playerObj, bandage);
	ISTimedActionQueue.add(ISFSBandageAction:new(playerObj, bandage, true));
end

ISForScience.dissectCorpseContextMenu = function(player, context, worldobjects, test)
	local playerObj = getSpecificPlayer(player)	
	local x = getMouseX()
	local y = getMouseY()

	local body = IsoObjectPicker.Instance:PickCorpse(x, y)
	if not body then return end

	if body then
		local doctorLevel = playerObj:getPerkLevel(Perks.Doctor)
		if doctorLevel >= 4 and doctorLevel < 6 then
			if test == true then return true; end
			local option = context:addOption(getText("ContextMenu_FSDissect_Corpse"), worldobjects, ISForScience.onDissectCorpse, player, body);
			local toolTip = ISWorldObjectContextMenu.addToolTip();
			toolTip.description = getText("ContextMenu_FSDissect_Tooltip") ;
			option.toolTip = toolTip;
			if playerObj:getInventory():containsTypeEvalRecurse("Scalpel", predicateNotBroken) == false then
				toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_Require", getItemNameFromFullType("Base.Scalpel"));
				option.toolTip = toolTip;
				option.notAvailable = true
			end
			if playerObj:getInventory():containsTypeRecurse("Base.BookMedicalJournal") then
				toolTip.description = toolTip.description .. " <LINE> <RGB:0,1,0> " .. getText("ContextMenu_FSJournal_Tooltip") ;
			end
		end
	end
end
	
ISForScience.onDissectCorpse = function(worldobjects, player, corpse)
	local playerObj = getSpecificPlayer(player);
	local playerInv = playerObj:getInventory();
	if corpse:getSquare() and luautils.walkAdj(playerObj, corpse:getSquare()) then
		local scalpel = playerInv:getFirstTypeEvalRecurse("Base.Scalpel", predicateNotBroken);
		ISInventoryPaneContextMenu.transferIfNeeded(playerObj, scalpel);
		ISTimedActionQueue.add(ISFSDissectCorpseAction:new(playerObj, corpse, scalpel));
	end
end

ISForScience.tinkerWorldContextMenu = function(player, context, worldobjects, test)
	local playerObj = getSpecificPlayer(player)
	if(playerObj:getPerkLevel(Perks.Electricity) > 5) then
		return
	end

	local sq = clickedSquare;
	local deviceSimple = nil
	local deviceAdvanced = nil
	if sq then 
		if sq:getObjects() then
			for i=0,sq:getObjects():size() - 1 do
				local obj = sq:getObjects():get(i);
				if instanceof(obj, "IsoRadio") or instanceof(obj, "IsoTelevision") then
					deviceSimple = obj;
					break;
				end
				if obj:getSprite() then
					local spriteName = obj:getSprite():getName() or nil;
					if spriteName then
						--Jukebox
						if (spriteName == "recreational_01_0") or (spriteName == "recreational_01_1") then
							deviceSimple = obj;
							break;
						end
						-- Security Terminal
						if (spriteName == "security_01_0") or (spriteName == "security_01_1") then
							deviceAdvanced = obj;
							break;
						end
						-- Arcade
						if (spriteName == "recreational_01_16") or (spriteName == "recreational_01_17") 
							or (spriteName == "recreational_01_18") or (spriteName == "recreational_01_19") 
							or (spriteName == "recreational_01_20") or (spriteName == "recreational_01_21") 
							or (spriteName == "recreational_01_22") or (spriteName == "recreational_01_23") then
							deviceAdvanced = obj;
							break;
						end
						-- Computer
						if (spriteName == "appliances_com_01_72") or (spriteName == "appliances_com_01_73") 
							or (spriteName == "appliances_com_01_74") or (spriteName == "appliances_com_01_75") then
							deviceAdvanced = obj;
							break;
						end
						-- Radio (if not found by IsoRadio)
						-- e.g. appliances_radio_01_8
						if string.find(spriteName, "radio") then
							deviceSimple = obj;
							break;
						end
						-- Television (if not found by IsoTelevision)
						-- e.g. appliances_television_01_2
						if string.find(spriteName, "television") then
							deviceSimple = obj;
							break;
						end
						-- Ham radios
						-- e.g. appliances_com_01
						if string.find(spriteName, "appliances_com_01") then
							deviceSimple = obj;
							break;
						end
					end
				end
			end
		end
	end
	if deviceSimple then
		if test == true then return true; end
		local option = context:addOption(getText("ContextMenu_FSTinker"), worldobjects, ISForScience.onTinkerWorld, player, deviceSimple, true);
		local toolTip = ISWorldObjectContextMenu.addToolTip();
		toolTip.description = getText("ContextMenu_FSSimple");
		option.toolTip = toolTip;
		if playerObj:getInventory():containsTypeRecurse("Base.Screwdriver") == false then
			toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_Require", getItemNameFromFullType("Base.Screwdriver"));
			option.notAvailable = true;
		end
		if playerObj:getInventory():containsTypeRecurse("Base.ElectronicsScrap") == false then
			toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_Require", getItemNameFromFullType("Base.ElectronicsScrap"));
			option.notAvailable = true;
		end
		if playerObj:getInventory():containsTypeRecurse("Base.BookElectricManual") then
			toolTip.description = toolTip.description .. " <LINE> <RGB:0,1,0> " .. getText("ContextMenu_FSManual_Tooltip");
		end
		if playerObj:getPerkLevel(Perks.Electricity) >= 4 then
			toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_FSTooSkilled_Tooltip");
			option.notAvailable = true;
		end
	end
	if deviceAdvanced and (playerObj:getPerkLevel(Perks.Electricity) >= 4) then
		if test == true then return true; end
		local option = context:addOption(getText("ContextMenu_FSTinker"), worldobjects, ISForScience.onTinkerWorld, player, deviceAdvanced, false);
		local toolTip = ISWorldObjectContextMenu.addToolTip();
		toolTip.description = getText("ContextMenu_FSAdvanced");
		option.toolTip = toolTip;
		if playerObj:getInventory():containsTypeRecurse("Base.Screwdriver") == false then
			toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_Require", getItemNameFromFullType("Base.Screwdriver"));
			option.notAvailable = true;
		end
		if playerObj:getInventory():containsTypeRecurse("Base.ElectronicsScrap") == false then
			toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_Require", getItemNameFromFullType("Base.ElectronicsScrap"));
			option.notAvailable = true;
		end
		if playerObj:getInventory():containsTypeRecurse("Base.BookElectricManual") then
			toolTip.description = toolTip.description .. " <LINE> <RGB:0,1,0> " .. getText("ContextMenu_FSManual_Tooltip");
		end
	end
end
	
ISForScience.onTinkerWorld = function(worldobjects, player, device, isSimple)
	local playerObj = getSpecificPlayer(player);
	local playerInv = playerObj:getInventory();
	if(playerObj:getPerkLevel(Perks.Electricity) > 5) then
		return
	end
	if device:getSquare() and luautils.walkAdj(playerObj, device:getSquare()) then
		local screwdriver = playerInv:getFirstTypeEvalRecurse("Base.Screwdriver", predicateNotBroken);
		ISInventoryPaneContextMenu.transferIfNeeded(playerObj, screwdriver);
		ISTimedActionQueue.add(ISFSTinkerAction:new(playerObj, device, nil, screwdriver, isSimple));
	end
end

ISForScience.tinkerInvContextMenu = function(player, context, items)
	local playerObj = getSpecificPlayer(player);
	if(playerObj:getPerkLevel(Perks.Electricity) > 5) then
		return
	end
	items = ISInventoryPane.getActualItems(items)
	for _, item in ipairs(items) do
		if instanceof(item, "Radio") then
			if test == true then return true; end
			local option = context:addOption(getText("ContextMenu_FSTinker"), player, ISForScience.onTinkerInv, item, true);
			local toolTip = ISWorldObjectContextMenu.addToolTip();
			toolTip.description = getText("ContextMenu_FSSimple");
			option.toolTip = toolTip;
			if playerObj:getInventory():containsTypeRecurse("Base.Screwdriver") == false then
				toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_Require", getItemNameFromFullType("Base.Screwdriver"));
				option.notAvailable = true;
			end
			if playerObj:getInventory():containsTypeRecurse("Base.ElectronicsScrap") == false then
				toolTip.description = toolTip.description .. " <LINE> <RGB:1,0,0> " .. getText("ContextMenu_Require", getItemNameFromFullType("Base.ElectronicsScrap"));
				option.notAvailable = true;
			end
			if playerObj:getInventory():containsTypeRecurse("Base.BookElectricManual") then
				toolTip.description = toolTip.description .. " <LINE> <RGB:0,1,0> " .. getText("ContextMenu_FSManual_Tooltip");
			end
			break;
		end
	end
end
	
ISForScience.onTinkerInv = function(player, device)
	local playerObj = getSpecificPlayer(player);
	local playerInv = playerObj:getInventory();
	local screwdriver = playerInv:getFirstTypeEvalRecurse("Base.Screwdriver", predicateNotBroken);
	ISInventoryPaneContextMenu.transferIfNeeded(playerObj, screwdriver);
	ISInventoryPaneContextMenu.transferIfNeeded(playerObj, device);
	ISTimedActionQueue.add(ISFSTinkerAction:new(playerObj, nil, device, screwdriver, true));
end

ISForScience.maintainContextMenu = function(player, context, items)
	local playerObj = getSpecificPlayer(player)
	local subMenuMaintain = nil;

	items = ISInventoryPane.getActualItems(items)
	for _, item in ipairs(items) do
		if item:getCategory() == "Weapon" then
			if (subMenuMaintain == nil) then
				subMenuMaintain = context:getNew(context);
				context:addSubMenu(context:addOption(getText("ContextMenu_FSMaintain")), subMenuMaintain);
			end
			
			local option = subMenuMaintain:addOption(getText("ContextMenu_FSMaintain"), player, ISForScience.onMaintain, item); 
			local conditionColor = "<RGB:1,1,1> ";
			if item:isBroken() then conditionColor = "<RGB:1,0,0> "; end
			local toolTip = ISInventoryPaneContextMenu.addToolTip();
			toolTip.description = getText("ContextMenu_FSMaintain_Tooltip");
			toolTip.description = toolTip.description .. " <LINE> " .. conditionColor .. getText("ContextMenu_FSCondition") .. ": " .. item:getCondition() .. " / " .. item:getConditionMax();
			toolTip.description = toolTip.description .. " <LINE> <RGB:1,1,1> " .. getText("ContextMenu_FSBloodLevel") .. ": " .. math.ceil(item:getBloodLevel()) .. " / 100";
			option.toolTip = toolTip;
		end
	end
end
	
ISForScience.onMaintain = function(player, weapon)
	local playerObj = getSpecificPlayer(player);
	local playerInv = playerObj:getInventory();
	if weapon:getCategory() == "Weapon" and playerInv:containsRecursive(weapon) then
		ISInventoryPaneContextMenu.equipWeapon(weapon, true, false, player);
		ISTimedActionQueue.add(ISFSMaintainAction:new(playerObj, weapon));
	end
end

Events.OnFillInventoryObjectContextMenu.Add(ISForScience.bandageContextMenu);
Events.OnFillWorldObjectContextMenu.Add(ISForScience.dissectCorpseContextMenu);
Events.OnFillWorldObjectContextMenu.Add(ISForScience.tinkerWorldContextMenu);
Events.OnFillInventoryObjectContextMenu.Add(ISForScience.tinkerInvContextMenu);
Events.OnFillInventoryObjectContextMenu.Add(ISForScience.maintainContextMenu);