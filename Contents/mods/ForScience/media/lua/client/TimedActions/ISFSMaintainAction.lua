require "TimedActions/ISBaseTimedAction"

ISFSMaintainAction = ISBaseTimedAction:derive("ISFSMaintainAction");

function ISFSMaintainAction:isValid()
	return self.weapon ~= nil and self.character:getInventory():contains(self.weapon);
end

function ISFSMaintainAction:update()
	self.weapon:setJobDelta(self:getJobDelta());	
end

function ISFSMaintainAction:start()
	if  self.weapon:getBloodLevel() <= 0 then
		self.character:Say(getText("IGUI_PlayerText_FSCantFix" .. (ZombRand(2)+1)), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
		self:forceComplete();
	else
		self.weapon:setJobType(getText("IGUI_JobType_FSMaintain"));
		self.weapon:setJobDelta(0.0);

		self:setActionAnim("Loot")
		self:setAnimVariable("LootPosition", "");
		self:setOverrideHandModels(self.weapon, nil);
		self.character:playSound("unlockDoor");
	end
end

function ISFSMaintainAction:stop()
	self.weapon:setJobDelta(0.0);

	ISBaseTimedAction.stop(self)
end

function ISFSMaintainAction:perform()
	self.weapon:setJobDelta(0.0);
	
	-- needed to remove from queue / start next.
	ISBaseTimedAction.perform(self)
end

function ISFSMaintainAction:animEvent(event, parameter)
	if event == 'ActiveAnimLooped' then	
		if ZombRand(10) == 0 then
			self.character:playSound("unlockDoor");
		end
		local bloodLevel = self.weapon:getBloodLevel();
		if bloodLevel > 0.0 then
			self.weapon:setBloodLevel(bloodLevel - 0.01);
			if (self.weapon:getBloodLevel() <= 0) then
				self.weapon:setBloodLevel(0);
				self.character:getXp():AddXP(Perks.Maintenance, 10);
				self.character:Say(getText("IGUI_PlayerText_FSMaintainBlood"), 0.55, 0.55, 0.55, UIFont.Dialogue, 0, "default");
				self:forceComplete();
			end
		end
	end
end

function ISFSMaintainAction:new (character, weapon)
	local o = {}
	setmetatable(o, self)
	self.__index = self
	o.character = character;
	o.weapon = weapon;
	o.maintenanceLvl = character:getPerkLevel(Perks.Maintenance);
	o.stopOnWalk = true;
	o.stopOnRun = true;
	o.maxTime = -1;
	o.forceProgressBar = true;
	return o
end