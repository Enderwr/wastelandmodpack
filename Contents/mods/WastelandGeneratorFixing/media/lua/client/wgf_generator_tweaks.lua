require "TimedActions/ISFixGenerator"

local stockNew = ISFixGenerator.new;
local stockPerform = ISFixGenerator.perform;

function ISFixGenerator:new(character, generator, time)
    local o = stockNew(self, character, generator, time);

    -- Add max fix to be 50 + 5 per perk level, up to 100
    o.maxFix = 50 + (o.character:getPerkLevel(Perks.Electricity) * 5);
    if o.maxFix > 100 or isAdmin() then o.maxFix = 100 end

	return o;
end

function ISFixGenerator:perform()
    -- check if generator condition is already at maxFix and say a message if so
    if self.generator:getCondition() >= self.maxFix and self.generator:getCondition() < 100 then
        self.character:Say(getText("UI_NeedMoreSkillToFix"));
        self:stop();
        return;
    end

    stockPerform(self);

    -- if a fix was performed, limit the condition to maxFix
    if self.generator:getCondition() >= self.maxFix then
        self.generator:setCondition(self.maxFix);
    end
end