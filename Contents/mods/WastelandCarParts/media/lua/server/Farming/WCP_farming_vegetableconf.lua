local stock_getObjectName = farming_vegetableconf.getObjectName;

local WCP_plantNames = {["WCPRubberBush"]=true};

farming_vegetableconf.getObjectName = function(plant)
    if not WCP_plantNames[plant.typeOfSeed] then
        return stock_getObjectName(plant);
    end

    if plant.state == "plow" then return getText("Farming_Plowed_Land") end
    if plant.state == "destroy" then return getText("Farming_Destroyed") .. " " .. getText("Farming_" .. plant.typeOfSeed) end
    if plant.state == "dry" then return getText("Farming_Receding") .. " " .. getText("Farming_" .. plant.typeOfSeed) end
    if plant.state == "rotten" then return getText("Farming_Rotten") .. " " .. getText("Farming_" .. plant.typeOfSeed) end
    if plant.nbOfGrow <= 1 then
        return getText("Farming_Seedling") .. " " .. getText("Farming_" ..plant.typeOfSeed);
    elseif plant.nbOfGrow <= 4 then
        return getText("Farming_Young") .. " " .. getText("Farming_" ..plant.typeOfSeed);
    elseif plant.nbOfGrow == 5 then
        return getText("Farming_Ready_for_Harvest") .. " " .. getText("Farming_" ..plant.typeOfSeed);
    elseif plant.nbOfGrow == 6 then
        return getText("Farming_Seed-bearing") .. " " .. getText("Farming_" ..plant.typeOfSeed);
    end
    return "Mystery Plant"
end

--Icons

farming_vegetableconf.icons["WCPRubberBush"] = "Item_CuredRubber";

-- Item_RubberBush

farming_vegetableconf.props["WCPRubberBush"] = farming_vegetableconf.props["WCPRubberBush"] or {}
farming_vegetableconf.props["WCPRubberBush"].ModFarmCrop = true;
farming_vegetableconf.props["WCPRubberBush"].seedsRequired = 2;
farming_vegetableconf.props["WCPRubberBush"].texture = "wcp_rubberbush_5";
farming_vegetableconf.props["WCPRubberBush"].waterLvl = 45;
farming_vegetableconf.props["WCPRubberBush"].timeToGrow = ZombRand(56,62);
farming_vegetableconf.props["WCPRubberBush"].vegetableName = "Base.RawRubber";
farming_vegetableconf.props["WCPRubberBush"].seedName = "Base.RubberBushSeed";
farming_vegetableconf.props["WCPRubberBush"].growCode = "ModFarm.growRubberBush";
farming_vegetableconf.props["WCPRubberBush"].harvestCode = "ModFarm.harvestRubberBush";
farming_vegetableconf.props["WCPRubberBush"].seedPerVeg = 1;
farming_vegetableconf.props["WCPRubberBush"].minVeg = 2;
farming_vegetableconf.props["WCPRubberBush"].maxVeg = 6;
farming_vegetableconf.props["WCPRubberBush"].minVegAutorized = 6;
farming_vegetableconf.props["WCPRubberBush"].maxVegAutorized = 8;
farming_vegetableconf.props["WCPRubberBush"].waterConsumption = 2;
if (getActivatedMods():contains("Hydrocraft")) then
    farming_vegetableconf.props["WCPRubberBush"].waterLvlMax = 100;
    farming_vegetableconf.props["WCPRubberBush"].minTemp = 5;
    farming_vegetableconf.props["WCPRubberBush"].bestTemp = 15;
    farming_vegetableconf.props["WCPRubberBush"].maxTemp = 30;
    farming_vegetableconf.props["WCPRubberBush"].plantWithFruit = false;
    farming_vegetableconf.props["WCPRubberBush"].damageFromStorm = false;
    farming_vegetableconf.props["WCPRubberBush"].multiHarvest = false;
    farming_vegetableconf.props["WCPRubberBush"].vegetableName2 = "";
    farming_vegetableconf.props["WCPRubberBush"].numberOfVegetables2 = 0;
end
if (getActivatedMods():contains("LeGourmetRevolution")) then
    farming_vegetableconf.props["WCPRubberBush"].sheetropeRequired = 0;
    farming_vegetableconf.props["WCPRubberBush"].sticksRequired = 0;
end

farming_vegetableconf.sprite["WCPRubberBush"] = {
"wcp_rubberbush_0",
"wcp_rubberbush_1",
"wcp_rubberbush_2",
"wcp_rubberbush_3",
"wcp_rubberbush_4",
"wcp_rubberbush_5",
"wcp_rubberbush_6",
"wcp_rubberbush_7",
}
