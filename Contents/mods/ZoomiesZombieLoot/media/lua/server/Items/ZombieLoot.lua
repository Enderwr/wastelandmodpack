require 'Items/SuburbsDistributions'

---@param item string
---@param weight number
local function addLootItem(item, weight)
    table.insert(SuburbsDistributions["all"]["inventorymale"].items, item);
    table.insert(SuburbsDistributions["all"]["inventorymale"].items, weight);
    table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, item);
    table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, weight);
end

-- Farming
addLootItem("farming.BroccoliBagSeed", 0.05)
addLootItem("farming.CabbageBagSeed", 0.05)
addLootItem("farming.CarrotBagSeed", 0.05)
addLootItem("farming.PotatoBagSeed", 0.05)
addLootItem("farming.RedRadishBagSeed", 0.05)
addLootItem("farming.StrewberrieBagSeed", 0.05)
addLootItem("farming.TomatoBagSeed", 0.05)

if getActivatedMods():contains("WastelandCarParts") then
    addLootItem("Base.RubberBushSeedBag", 0.05)
end

if getActivatedMods():contains("AnaLGiNs_RenewableFoodResources") then
    addLootItem("ANL.SugarBeetBagSeed", 0.05)
end

-- Food and water
addLootItem("Base.Peanuts", 0.2)
addLootItem("Base.Peanuts", 0.2)
addLootItem("Base.SunflowerSeeds", 0.4)
addLootItem("Base.Crisps", 0.1)
addLootItem("Base.Crisps2", 0.1)
addLootItem("Base.Crisps3", 0.1)
addLootItem("Base.MintCandy", 0.2)
addLootItem("Base.WaterBottleFull", 0.1)
addLootItem("Base.WaterBottleEmpty", 0.2)
addLootItem("Base.Chocolate", 0.1)
addLootItem("Base.Lollipop", 0.1)
addLootItem("Base.TortillaChips", 0.1)
addLootItem("Base.GranolaBar", 0.3)
addLootItem("Base.BeerBottle", 0.1)
addLootItem("Base.BeerCan", 0.05)
addLootItem("Base.CookiesOatmeal", 0.3)
addLootItem("Base.Wine", 0.1)
addLootItem("Base.Wine2", 0.1)

    -- Additional Alcohol/Beverages (SapphCooking)
if getActivatedMods():contains("sapphcooking") then
    addLootItem("SapphCooking.SakeFull", 0.1)
    addLootItem("SapphCooking.VodkaFull", 0.1)
    addLootItem("SapphCooking.TequillaFull", 0.1)
    addLootItem("SapphCooking.RumFull", 0.1)
    addLootItem("SapphCooking.CachaçaFull", 0.1)
    addLootItem("SapphCooking.ColaBottle", 0.1)
    addLootItem("SapphCooking.CarbonatedWater",0.1)
end

-- Consumables
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Battery");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Battery");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Tissue");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Tissue");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Newspaper");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 4);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Newspaper");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Nails");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 2.2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Nails");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.7);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Screws");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 1.3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Screws");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 1.1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.ScrapMetal");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.ScrapMetal");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 3);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.UnusableMetal");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.UnusableMetal");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.EngineParts");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.01);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.EngineParts");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.01);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.PetrolCan");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.PetrolCan");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.DuctTape");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.DuctTape");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Glue");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.5);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Glue");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.5);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Woodglue");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Woodglue");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Bleach");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Bleach");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Matches");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.5);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Matches");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.5);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Candle");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Candle");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.HottieZ");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Magazine");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.LightBulb");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.LightBulb");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 3);

-- Medical
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.WhiskeyFull");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.WhiskeyFull");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.3);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Tweezers");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Tweezers");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 1);

-- Guns
addLootItem("Base.Pistol", 0.0001) -- M9 Pistol (uses 9mm ammo)
addLootItem("Base.Pistol2", 0.0001) -- M1911 Pistol (uses .45 ammo)
addLootItem("Base.ShotgunSawnoff", 0.0001) -- Sawed-off variety of the JS-2000 shotgun, aka Mossberg 500 in Firearms B41
addLootItem("Base.Revolver_Short", 0.0002) -- M36 Revolver (uses .38 ammo)

-- Ammo
addLootItem("Base.Bullets38", 0.03) -- Interchangeable with .357
addLootItem("Base.ShotgunShells", 0.015)
addLootItem("Base.Bullets9mm", 0.03)
addLootItem("Base.308Bullets", 0.004) -- Interchangeable with 7.62x51mm 
addLootItem("Base.Bullets45", 0.003)

-- B41 Ammo
if getActivatedMods():contains("firearmmod") then
    addLootItem("762x39Bullets", 0.0012)
    addLootItem("762x51Bullets", 0.0012) -- Interchangeable with .308
    addLootItem("223Bullets", 0.0012) -- Interchangeable with 5.56x45mm (556Bullets)
    addLootItem("Bullets3006" , 0.0012)
    addLootItem("Bullets22", 0.003)
    addLootItem("556Bullets", 0.0012) -- Interchangeable with .223
    addLootItem("Bullets357", 0.003) -- NOT interchangeable with .38 (there is no recipe for now)
    addLootItem("Bullets4440", 0.003)
    addLootItem("Bullets44", 0.003)
end

-- Weapons
addLootItem("Base.Machete", 0.00001)
addLootItem("Base.MeatCleaver", 0.009)


-- Trashy Weapons
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.BreadKnife");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.7);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.BreadKnife");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.7);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.ButterKnife");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.ButterKnife");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 3);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Scalpel");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.5);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Scalpel");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Stake");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 1);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Stake");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.RollingPin");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.8);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.RollingPin");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.8);

-- Cooking
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Pepper");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.005);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Pepper");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.005);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Pot");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Pot");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Pan");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.03);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Pan");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.03);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Saucepan");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.04);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Saucepan");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.04);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.KitchenKnife");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.4);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.KitchenKnife");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.5);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.EmptyJar");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.4);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.EmptyJar");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.5);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.JarLid");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.JarLid");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 2);


-- Tools
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.TinOpener");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.3);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.TinOpener");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.3);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Hammer");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Hammer");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.PipeWrench");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.01);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.PipeWrench");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.03);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Spanner");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.05);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Spanner");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.01);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Saw");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.05);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Saw");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.05);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.HandAxe");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.HandAxe");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Axe");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Axe");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.WoodAxe");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.WoodAxe");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Crowbar");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.0003);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Crowbar");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.0003);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Sledgehammer");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Sledgehammer");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.0001);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.BlowTorch");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.0005);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.BlowTorch");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.0005);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Torch");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Torch");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Radio.WalkieTalkie1");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Radio.WalkieTalkie1");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Radio.WalkieTalkie2");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.01);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Radio.WalkieTalkie2");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.01);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Earbuds");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Earbuds");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.1);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Pen");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Pen");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 2);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.EmptySandbag");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.EmptySandbag");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Twine");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.7);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Twine");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.7);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Thread");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 4);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Thread");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 4);

-- Containers
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Bag_FannyPackBack");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.002);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Bag_FannyPackFront");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.004);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Plasticbag");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.7);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Plasticbag");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.7);

-- Warm Clothing
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Hat_Beany");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.05);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Hat_Beany");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.05);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Scarf_StripeBlackWhite");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Scarf_StripeBlackWhite");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Scarf_StripeRedWhite");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Scarf_StripeRedWhite");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Scarf_StripeBlueWhite");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Scarf_StripeBlueWhite");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Scarf_White");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 0.02);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Scarf_White");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 0.02);

-- Misc
table.insert(SuburbsDistributions["all"]["inventorymale"].items, "Base.Money");
table.insert(SuburbsDistributions["all"]["inventorymale"].items, 9);
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, "Base.Money");
table.insert(SuburbsDistributions["all"]["inventoryfemale"].items, 9);
