---
--- professions.lua
--- Wasteland Server Professions
--- 10/11/2022
---
ProfessionFramework.RemoveDefaultProfessions = true
ProfessionFramework.RemoveDefaultTraits = true

ProfessionFramework.addProfession('unemployed', {
    icon = "profession_unemployed",
    name = getText("UI_prof_unemployed"),
    cost = 4,
    inventory = {
        ["Base.Crisps"] = 1,
    },
})

ProfessionFramework.addProfession('parkranger', {
    icon = "profession_survivalist",
    name = getText("UI_prof_survivalist"),
    cost = -12,
    xp = {
        [Perks.Trapping] = 2,
        [Perks.Fishing] = 1,
        [Perks.Spear] = 1,
        [Perks.PlantScavenging] = 3,
        [Perks.Woodwork] = 1,
    },
    recipes = {"Make Stick Trap", "Make Snare Trap", "Make Wooden Cage Trap", "Make Trap Box", "Make Cage Trap",
        "Craft Wooden Crossbow"},
    clothing = {
        Hat = {"Hat_WoolyHat"},
        Sweater = {"Jumper_RoundNeck"},
        Pants = {"TrousersMesh_DenimLight"}
    },
    inventory = {
        ["Base.Matches"] = 1,
        ["Base.Saucepan"] = 1,
        ["Base.Spoon"] = 1,
    },
    traits = {
        "Herbalist2",
    },
})

ProfessionFramework.addProfession('securityguard', {
    icon = "profession_riotcop",
    name = getText("UI_prof_riotpolice"),
    cost = -8,
    xp = {
        [Perks.SmallBlunt] = 3,
        [Perks.Nimble] = 2,
        [Perks.Strength] = 1,
        [Perks.Aiming] = 1,
    },
    clothing = {
        Pant = {"Base.Trousers_Police"},
        Tshirt = {"Tshirt_Profession_PoliceBlue"},
        Shoes = {"Base.Shoes_Black"}
    },
})

ProfessionFramework.addProfession('fireofficer', {
    icon = "profession_fireofficer2",
    name = getText("UI_prof_fireoff"),
    cost = -6,
    xp = {
        [Perks.Axe] = 1,
        [Perks.Fitness] = 2,
        [Perks.Sprinting] = 1,
        [Perks.Strength] = 2,
    },
    clothing = {
        Tshirt = {"Base.Tshirt_Profession_FiremanRed"},
    },
    traits = {
        "Brave2",
    },
    inventory = {
        ["Base.Extinguisher"] = 1,
    },
})

ProfessionFramework.addProfession('constructionworker', {
    icon = "profession_convict",
    name = getText("UI_prof_convict"),
    cost = -2,
    xp = {
        [Perks.Woodwork] = 1,
        [Perks.Strength] = 1,
        [Perks.SmallBlade] = 1,
    },
    traits = {
        "ThickSkinned2"
    },
    clothing = {
        TankTop = {"Base.Vest_DefaultTEXTURE"},
        FullSuit = {"Base.Boilersuit_Prisoner"},
        Shoes = {"Base.Shoes_TrainerTINT"},
    },
    inventory = {
        ["Base.Stake"] = 1,
    },
})

ProfessionFramework.addProfession('carpenter', {
    icon = "profession_hammer2",
    name = getText("UI_prof_Carpenter"),
    cost = -2,
    xp = {
        [Perks.Woodwork] = 3,
        [Perks.SmallBlunt] = 2,
    },
    recipes = { "Make Trap Box", "Craft Wooden Crossbow"},
    inventory = {
        ["Base.Saw"] = 1,
        ["Base.Nails"] = 4,
    },
})

ProfessionFramework.addProfession('burglar', {
    icon = "profession_burglar2",
    name = getText("UI_prof_Burglar"),
    cost = -4,
    xp = {
        [Perks.Nimble] = 2,
        [Perks.Lightfoot] = 2,
        [Perks.Sneak] = 2,
    },
    traits = {
        "Burglar",
        "Graceful2"
    },
})

ProfessionFramework.addProfession('chef', {
    icon = "profession_chef2",
    name = getText("UI_prof_Chef"),
    cost = -4,
    xp = {
        [Perks.Cooking] = 3,
        [Perks.Maintenance] = 1,
        [Perks.SmallBlade] = 1,
    },
    recipes = { "Make Cake Batter", "Make Pie Dough", "Make Bread Dough", "Make Biscuits",
                "Make Chocolate Cookie Dough", "Make Chocolate Chip Cookie Dough", "Make Oatmeal Cookie Dough",
                "Make Shortbread Cookie Dough", "Make Sugar Cookie Dough", "Make Pizza"},
    traits = {
        "Cook2", "Dextrous2", "Nutritionist2"
    },
})

ProfessionFramework.addProfession('farmer', {
    icon = "profession_farmer2",
    name = getText("UI_prof_Farmer"),
    cost = 0,
    xp = {
        [Perks.Farming] = 3,
        [Perks.Spear] = 1,
        [Perks.Maintenance] = 1,
    },
    recipes = { "Make Flies Cure", "Make Mildew Cure" },
    inventory = {
        ["farming.HandShovel"] = 1,
        ["farming.PotatoSeed"] = 8,
    },
})

ProfessionFramework.addProfession('fisherman', {
    icon = "profession_fisher2",
    name = getText("UI_prof_Fisherman"),
    cost = -4,
    xp = {
        [Perks.Fishing] = 3,
        [Perks.PlantScavenging] = 1,
        [Perks.Spear] = 1,
    },
    recipes = { "Make Fishing Rod", "Fix Fishing Rod", "Get Wire Back", "Make Fishing Net"},
    inventory = {
        ["Base.SpearCrafted"] = 1,
    },
})

ProfessionFramework.addProfession('doctor', {
    icon = "profession_doctor2",
    name = getText("UI_prof_Doctor"),
    cost = -2,
    xp = {
        [Perks.Doctor] = 3,
        [Perks.SmallBlade] = 1,
    },
    inventory = {
        ["Base.Scalpel"] = 1,
        ["Base.CottonBalls"] = 1,
        ["Base.Pills"] = 1,
    },
})

ProfessionFramework.addProfession('nurse', {
    icon = "profession_paramedic",
    name = getText("UI_prof_Paramedic"),
    cost = -2,
    xp = {
        [Perks.Doctor] = 2,
        [Perks.Fitness] = 1,
        [Perks.Sprinting] = 1,
    },
    inventory = {
        ["Base.AlcoholBandage"] = 2,
    },
})

ProfessionFramework.addProfession('engineer', {
    icon = "profession_engineer",
    name = getText("UI_prof_Engineer"),
    cost = -2,
    xp = {
        [Perks.Electricity] = 1,
        [Perks.Mechanics] = 1,
        [Perks.MetalWelding] = 1,
        [Perks.Maintenance] = 1,
    },
    recipes = { "Make Aerosol bomb", "Make Flame bomb", "Make Pipe bomb", "Make Noise generator", "Make Smoke Bomb",
                "Craft Hand Crossbow", "MakeEnginePart"},
    inventory = {
        ["Base.Screwdriver"] = 1,
    },
})

ProfessionFramework.addProfession('metalworker', {
    icon = "profession_metalworker",
    name = getText("UI_prof_MetalWorker"),
    cost = -1,
    xp = {
        [Perks.MetalWelding] = 3,
    },
    recipes = { "Make Metal Walls", "Make Metal Fences", "Make Metal Containers", "Make Metal Sheet",
                "Make Small Metal Sheet", "Make Metal Roof", "Craft Metal Crossbow"},
    inventory = {
        ["Base.Screws"] = 2,
    },
})

ProfessionFramework.addProfession('veteran', {
    icon = "profession_mercenary",
    name = getText("UI_prof_Mercenary"),
    cost = -12,
    xp = {
        [Perks.Aiming] = 3,
        [Perks.Reloading] = 3,
        [Perks.Fitness] = 1,
        [Perks.Strength] = 1,
    },
    traits = {
        "Desensitized",
    },
    clothing = {
        TorsoExtra = {"Base.Vest_Hunting_Camo"},
    },
    inventory = {
        ["Base.HolsterSimple"] = 1,
    },
})

ProfessionFramework.addProfession('lumberjack', {
    icon = "profession_lumberjack",
    name = getText("UI_prof_Lumberjack"),
    cost = -4,
    xp = {
        [Perks.Axe] = 3,
        [Perks.Strength] = 2,
    },
    traits = {
        "Axeman",
    },
})

ProfessionFramework.addProfession('mechanics', {
    icon = "profession_mechanic",
    name = getText("UI_prof_Mechanics"),
    cost = -4,
    xp = {
        [Perks.Mechanics] = 3,
        [Perks.SmallBlunt] = 1,
    },
    recipes = { "Basic Mechanics", "Intermediate Mechanics", "Advanced Mechanics" },
    traits = {
        "Mechanics2",
    },
    inventory = {
        ["Base.LugWrench"] = 1,
    },
})

ProfessionFramework.addProfession('policeofficer', {
    icon = "profession_peacekeeper",
    name = getText("UI_prof_peacekeeper"),
    cost = -6,
    xp = {
        [Perks.Aiming] = 2,
        [Perks.Reloading] = 2,
        [Perks.Doctor] = 1,
        [Perks.Fitness] = 1,
    },
})

ProfessionFramework.addProfession('tailor', {
    icon = "profession_tailor",
    name = getText("UI_prof_tailor"),
    cost = -3,
    xp = {
        [Perks.Tailoring] = 3,
    },
    inventory = {
        ["Base.Needle"] = 1,
        ["Base.Thread"] = 1,
        ["Base.Scissors"] = 1,
    },
    traits = {
        "Dextrous2"
    },
})

ProfessionFramework.addProfession('electrician', {
    icon = "profession_electrician",
    name = getText("UI_prof_Electrician"),
    cost = 0,
    xp = {
        [Perks.Electricity] = 3,
        [Perks.MetalWelding] = 1,
    },
    recipes = { "Generator", "Make Remote Controller V1", "Make Remote Controller V2", "Make Remote Controller V3",
                "Make Remote Trigger", "Make Timer", "Craft Makeshift Radio", "Craft Makeshift HAM Radio",
                "Craft Makeshift Walkie Talkie"
    },
    inventory = {
        ["Base.Screwdriver"] = 1,
    },
})

ProfessionFramework.addProfession('burgerflipper', {
    icon = "profession_homemaker",
    name = getText("UI_prof_Homemaker"),
    cost = -4,
    xp = {
        [Perks.Cooking] = 2,
        [Perks.Tailoring] = 1,
    },
    recipes = { "Make Cake Batter", "Make Pie Dough", "Make Bread Dough", "Make Biscuits",
                "Make Chocolate Cookie Dough", "Make Chocolate Chip Cookie Dough", "Make Oatmeal Cookie Dough",
                "Make Shortbread Cookie Dough", "Make Sugar Cookie Dough", "Make Pizza"},
    traits = {
        "Cook2", "Organized2"
    },
    inventory = {
        ["Base.Apron_White"] = 1,
        ["Base.RollingPin"] = 1,
        ["Base.Scissors"] = 1,
    },
})

ProfessionFramework.addProfession('fitnessInstructor', {
    icon = "profession_fitnessinstructor",
    name = getText("UI_prof_FitnessInstructor"),
    cost = -4,
    xp = {
        [Perks.Fitness] = 3,
        [Perks.Sprinting] = 2,
    },
    traits = {
        "Nutritionist2", "Graceful2"
    },
})

-- New professions not just overriding existing ones

ProfessionFramework.addProfession('racer', {
    icon = "profession_racer",
    name = getText("UI_prof_Racer"),
    cost = -2,
    xp = {
        [Perks.Mechanics] = 1,
    },
    recipes = { "Advanced Mechanics" },
    traits = {
        "AdrenalineJunkie2", "SpeedDemon2", "Motorhead"
    },
    inventory = {
        ["Base.TirePump"] = 1,
    },
})

ProfessionFramework.addProfession('homeless', {
    icon = "profession_homeless",
    name = getText("UI_prof_Homeless"),
    cost = 0,
    xp = {
        [Perks.PlantScavenging] = 2,
        [Perks.Tailoring] = 1,
    },
    inventory = {
        ["Base.Garbagebag"] = 1,
        ["Base.TinCanEmpty"] = 1,
        ["Base.Toothbrush"] = 1,
        ["Base.Spoon"] = 1,
        ["Base.Bowl"] = 1,
    },
    traits = {
        "IronGut2", "DumpsterDiver"
    },
})

ProfessionFramework.addProfession('librarian', {
    icon = "profession_librarian",
    name = getText("UI_prof_Librarian"),
    cost = 0,
    xp = {
        [Perks.Lightfoot] = 1,
        [Perks.Sneak] = 1,
    },
    inventory = {
        ["Base.Book"] = 2,
        ["Base.Pencil"] = 1,
    },
    recipes = {
        "Basic Mechanics", "Make Fishing Rod", "Fix Fishing Rod", "Generator", "Craft Makeshift Radio", "Make Metal Sheet", "Make Mildew Cure", "Make Biscuits", "Make Pizza", "Make Stick Trap",
    },
    traits = {
        "FastReader2", "ShortSighted2", "WellRead"
    },
})


-- New Wasteland Traits
ProfessionFramework.addTrait("Small", {
    name = "UI_trait_small",
    description = "UI_trait_smalldesc",
    cost = -4,
    xp = {
        [Perks.Strength] = -2,
        [Perks.Nimble] = 1,
        [Perks.Sneak] = 1
},
    exclude = { "Weak" },
})

ProfessionFramework.addTrait("Gunner", {
    name = "UI_trait_gunner",
    description = "UI_trait_gunnerdesc",
    cost = 5,
    xp = {
        [Perks.Aiming] = 1,
        [Perks.Reloading] = 1
    },
    exclude = { "Pacifist" },
})

ProfessionFramework.addTrait("Yogi", {
    name = "UI_trait_yogi",
    description = "UI_trait_yogidesc",
    cost = 5,
    xp = {
        [Perks.Sprinting] = 1,
        [Perks.Lightfoot] = 1,
        [Perks.Nimble] = 1
    },
    exclude = { "Clumsy" },
})

ProfessionFramework.addTrait("ExplosivesExpert", {
    name = "UI_trait_explosives",
    description = "UI_trait_explosivesdesc",
    cost = 1,
    xp = {
        [Perks.Electricity] = 1,
    },
    recipes = { "Make Aerosol bomb", "Make Flame bomb", "Make Pipe bomb", "Make Noise generator", "Make Smoke Bomb",
                "Make Remote Trigger" },
    exclude = { "Pacifist" },
})

ProfessionFramework.addTrait("DumpsterDiver", {
    name = "UI_trait_dumpster",
    description = "UI_trait_dumpsterDesc",
    profession = true,
})

ProfessionFramework.addTrait("WellRead", {
    name = "UI_trait_WellRead",
    description = "UI_trait_WellReadDesc",
    profession = true,
})

ProfessionFramework.addTrait("Motorhead", {
    name = "UI_trait_Motorhead",
    description = "UI_trait_MotorheadDesc",
    profession = true,
})

-- Vanilla Traits

ProfessionFramework.addTrait("AdrenalineJunkie", {
    name = "UI_trait_AdrenalineJunkie",
    description = "UI_trait_AdrenalineJunkieDesc",
    cost = 6,
    exclude = { "AdrenalineJunkie2" },
})

ProfessionFramework.addTrait("AdrenalineJunkie2", {
    name = "UI_trait_AdrenalineJunkie",
    description = "UI_trait_AdrenalineJunkieDesc",
    profession = true,
})

ProfessionFramework.addTrait("Athletic", {
    name = "UI_trait_athletic",
    description = "UI_trait_athleticdesc",
    cost = 10,
    xp = {
        [Perks.Fitness] = 4,
    },
    exclude = { "Overweight", "Fit", "Obese", "Out of Shape", "Unfit" },
})

ProfessionFramework.addTrait("Brave", {
    name = "UI_trait_brave",
    description = "UI_trait_bravedesc",
    cost = 2,
    exclude = { "Cowardly", "Brave2" },
})

ProfessionFramework.addTrait("Brave2", {
    name = "UI_trait_brave",
    description = "UI_trait_bravedesc",
    profession = true,
})

ProfessionFramework.addTrait("NightVision", {
    name = "UI_trait_NightVision",
    description = "UI_trait_NightVisionDesc",
    cost = 2,
})

ProfessionFramework.addTrait("Dextrous", {
    name = "UI_trait_Dexterous",
    description = "UI_trait_DexterousDesc",
    cost = 2,
    exclude = { "AllThumbs", "Dextrous2" },
})

ProfessionFramework.addTrait("Dextrous2", {
    name = "UI_trait_Dexterous",
    description = "UI_trait_DexterousDesc",
    profession = true,
    exclude = { "AllThumbs" },
})

ProfessionFramework.addTrait("EagleEyed", {
    name = "UI_trait_eagleeyed",
    description = "UI_trait_eagleeyeddesc",
    cost = 1,
    exclude = { "ShortSighted", "ShortSighted2" }
})

ProfessionFramework.addTrait("FastHealer", {
    name = "UI_trait_FastHealer",
    description = "UI_trait_FastHealerDesc",
    cost = 4,
    exclude = { "SlowHealer" },
})

ProfessionFramework.addTrait("FastLearner", {
    name = "UI_trait_FastLearner",
    description = "UI_trait_FastLearnerDesc",
    cost = 6,
    exclude = { "SlowLearner" },
})

ProfessionFramework.addTrait("FastReader", {
    name = "UI_trait_FastReader",
    description = "UI_trait_FastReaderDesc",
    cost = 2,
})

ProfessionFramework.addTrait("FastReader2", {
    name = "UI_trait_FastReader",
    description = "UI_trait_FastReaderDesc",
    profession = true,
    exclude = { "FastReader" },
})

ProfessionFramework.addTrait("Fit", {
    name = "UI_trait_fit",
    description = "UI_trait_fitdesc",
    cost = 6,
    xp = {
        [Perks.Fitness] = 2,
    },
    exclude = { "Out of Shape", "Unfit", "Overweight", "Obese"},
})

ProfessionFramework.addTrait("Graceful", {
    name = "UI_trait_graceful",
    description = "UI_trait_gracefuldesc",
    cost = 3,
    exclude = { "Graceful2" }
})

ProfessionFramework.addTrait("Graceful2", {
    name = "UI_trait_graceful",
    description = "UI_trait_gracefuldesc",
    profession = true,
})

ProfessionFramework.addTrait("Inconspicuous", {
    name = "UI_trait_Inconspicuous",
    description = "UI_trait_InconspicuousDesc",
    cost = 3,
})

ProfessionFramework.addTrait("IronGut", {
    name = "UI_trait_IronGut",
    description = "UI_trait_IronGutDesc",
    cost = 3,
})

ProfessionFramework.addTrait("IronGut2", {
    name = "UI_trait_IronGut",
    description = "UI_trait_IronGutDesc",
    profession = true,
    exclude = { "IronGut" },
})

ProfessionFramework.addTrait("KeenHearing", {
    name = "UI_trait_keenhearing",
    description = "UI_trait_keenhearingdesc",
    cost = 6,
})

ProfessionFramework.addTrait("LightEater", {
    name = "UI_trait_lighteater",
    description = "UI_trait_lighteaterdesc",
    cost = 4,
})

ProfessionFramework.addTrait("LowThirst", {
    name = "UI_trait_LowThirst",
    description = "UI_trait_LowThirstDesc",
    cost = 3,
    exclude = { "HighThirst" },
})

--[[
ProfessionFramework.addTrait("Lucky", {
    name = "UI_trait_lucky",
    description = "UI_trait_luckydesc",
    cost = 4,
    exclude = { "Unlucky" },
})
--]]

ProfessionFramework.addTrait("Organized", {
    name = "UI_trait_Packmule",
    description = "UI_trait_PackmuleDesc",
    cost = 6,
    exclude = { "Disorganized", "Organized2"},
})

ProfessionFramework.addTrait("Organized2", {
    name = "UI_trait_Packmule",
    description = "UI_trait_PackmuleDesc",
    profession = true,
    exclude = { "Disorganized" },
})

ProfessionFramework.addTrait("Outdoorsman", {
    name = "UI_trait_outdoorsman",
    description = "UI_trait_outdoorsmandesc",
    cost = 2,
})

ProfessionFramework.addTrait("Resilient", {
    name = "UI_trait_resilient",
    description = "UI_trait_resilientdesc",
    cost = 2,
    exclude = { "ProneToIllness" },
})

ProfessionFramework.addTrait("SpeedDemon", {
    name = "UI_trait_SpeedDemon",
    description = "UI_trait_SpeedDemonDesc",
    cost = 1,
})

ProfessionFramework.addTrait("SpeedDemon2", {
    name = "UI_trait_SpeedDemon",
    description = "UI_trait_SpeedDemonDesc",
    profession = true,
    exclude = { "SpeedDemon"},
})

ProfessionFramework.addTrait("Stout", {
    name = "UI_trait_stout",
    description = "UI_trait_stoutdesc",
    cost = 6,
    xp = {
        [Perks.Strength] = 2
    },
    exclude = { "Feeble" },
})

ProfessionFramework.addTrait("Strong", {
    name = "UI_trait_strong",
    description = "UI_trait_strongdesc",
    cost = 10,
    xp = {
        [Perks.Strength] = 4
    },
    exclude = { "Feeble", "Stout" },
})

ProfessionFramework.addTrait("ThickSkinned", {
    name = "UI_trait_thickskinned",
    description = "UI_trait_thickskinneddesc",
    cost = 4,
    exclude = { "Thinskinned" },
})

ProfessionFramework.addTrait("ThickSkinned2", {
    name = "UI_trait_thickskinned",
    description = "UI_trait_thickskinneddesc",
    profession = true,
    exclude = { "ThickSkinned", "Thinskinned" }
})

ProfessionFramework.addTrait("Mechanics", {
    name = "UI_trait_Mechanics",
    description = "UI_trait_MechanicsDesc",
    cost = 9,
    xp = {
        [Perks.Mechanics] = 2,
        [Perks.Maintenance] = 1,
    },
    recipes = { "Basic Mechanics", "Intermediate Mechanics" },
    exclude = { "Mechanics2" },
})

ProfessionFramework.addTrait("Mechanics2", {
    name = "UI_trait_Mechanics",
    description = "UI_trait_Mechanics2Desc",
    profession = true,
})

ProfessionFramework.addTrait("Fishing", {
    name = "UI_trait_Fishing",
    description = "UI_trait_FishingDesc",
    cost = 3,
    xp = {
        [Perks.Fishing] = 1
    },
    recipes = { "Make Fishing Rod", "Fix Fishing Rod" },
})

ProfessionFramework.addTrait("Axeman", {
    name = "UI_trait_axeman",
    description = "UI_trait_axemandesc",
    profession = true,
})

ProfessionFramework.addTrait("BaseballPlayer", {
    name = "UI_trait_PlaysBaseball",
    description = "UI_trait_PlaysBaseballDesc",
    cost = 6,
    xp = {
        [Perks.Blunt] = 2,
    },
})

ProfessionFramework.addTrait("Burglar", {
    name = "UI_prof_Burglar",
    description = "UI_trait_BurglarDesc",
    profession = true,
})

ProfessionFramework.addTrait("Brawler", {
    name = "UI_trait_BarFighter",
    description = "UI_trait_BarFighterDesc",
    cost = 6,
    xp = {
        [Perks.SmallBlade] = 1,
        [Perks.SmallBlunt] = 1,
        [Perks.Blunt] = 1,
    },
})

ProfessionFramework.addTrait("Cook", {
    name = "UI_trait_Cook",
    description = "UI_trait_CookDesc",
    cost = 5,
    xp = {
        [Perks.Cooking] = 2,
    },
    recipes = { "Make Cake Batter", "Make Pie Dough", "Make Bread Dough" },
    exclude = { "Cook2" },
})

ProfessionFramework.addTrait("Cook2", {
    name = "UI_trait_Cook",
    description = "UI_trait_Cook2Desc",
    profession = true,
})

ProfessionFramework.addTrait("Desensitized", {
    name = "UI_trait_Desensitized",
    description = "UI_trait_DesensitizedDesc",
    profession = true,
    exclude = { "Hemophobic", "Cowardly", "Brave", "Agoraphobic", "Claustophobic", "AdrenalineJunkie" },
})

ProfessionFramework.addTrait("FirstAid", {
    name = "UI_trait_FirstAid",
    description = "UI_trait_FirstAidDesc",
    cost = 3,
    xp = {
        [Perks.Doctor] = 1
    },
})

ProfessionFramework.addTrait("Formerscout", {
    name = "UI_trait_Scout",
    description = "UI_trait_ScoutDesc",
    cost = 5,
    xp = {
        [Perks.Doctor] = 1,
        [Perks.PlantScavenging] = 1,
    },
})

ProfessionFramework.addTrait("Gardener", {
    name = "UI_trait_Gardener",
    description = "UI_trait_GardenerDesc",
    cost = 3,
    xp = {
        [Perks.Farming] = 1,
        [Perks.PlantScavenging] = 1,
    },
    recipes = { "Make Mildew Cure", "Make Flies Cure" },
})

ProfessionFramework.addTrait("Gymnast", {
    name = "UI_trait_Gymnast",
    description = "UI_trait_GymnastDesc",
    cost = 6,
    xp = {
        [Perks.Nimble] = 2,
    },
})

ProfessionFramework.addTrait("Handy", {
    name = "UI_trait_handy",
    description = "UI_trait_handydesc",
    cost = 6,
    xp = {
        [Perks.Maintenance] = 1,
        [Perks.Woodwork] = 1,
    },
})

ProfessionFramework.addTrait("Herbalist", {
    name = "UI_trait_Herbalist",
    description = "UI_trait_HerbalistDesc",
    cost = 1,
    recipes = { "Herbalist" },
    exclude = { "Herbalist2"},
})

ProfessionFramework.addTrait("Herbalist2", {
    name = "UI_trait_Herbalist",
    description = "UI_trait_HerbalistDesc",
    profession = true,
    recipes = { "Herbalist" },
})

ProfessionFramework.addTrait("Hiker", {
    name = "UI_trait_Hiker",
    description = "UI_trait_HikerDesc",
    cost = 6,
    xp = {
        [Perks.PlantScavenging] = 2,
        [Perks.Trapping] = 1,
    },
    recipes = { "Make Stick Trap", "Make Snare Trap", "Make Wooden Cage Trap" },
})

ProfessionFramework.addTrait("Hunter", {
    name = "UI_trait_Hunter",
    description = "UI_trait_HunterDesc",
    cost = 7,
    xp = {
        [Perks.Aiming] = 1,
        [Perks.Trapping] = 1,
        [Perks.Sneak] = 1,
        [Perks.Lightfoot] = 1,
    },
    recipes = { "Make Stick Trap", "Make Snare Trap", "Make Wooden Cage Trap", "Make Trap Box", "Make Cage Trap" },
})

ProfessionFramework.addTrait("NightOwl", {
    name = "UI_trait_nightowl",
    description = "UI_trait_nightowldesc",
    profession = true,
})

ProfessionFramework.addTrait("Nutritionist", {
    name = "UI_trait_nutritionist",
    description = "UI_trait_nutritionistdesc",
    cost = 1,
    exclude = { "Nutritionist2" },
})

ProfessionFramework.addTrait("Nutritionist2", {
    name = "UI_trait_nutritionist",
    description = "UI_trait_nutritionistdesc",
    profession = true,
})

ProfessionFramework.addTrait("Jogger", {
    name = "UI_trait_Jogger",
    description = "UI_trait_JoggerDesc",
    cost = 4,
    xp = {
        [Perks.Sprinting] = 2
    },
})

ProfessionFramework.addTrait("Tailor", {
    name = "UI_trait_Tailor",
    description = "UI_trait_TailorDesc",
    cost = 6,
    xp = {
        [Perks.Tailoring] = 2
    },
})

ProfessionFramework.addTrait("Agoraphobic", {
    name = "UI_trait_agoraphobic",
    description = "UI_trait_agoraphobicdesc",
    cost = -6,
})

ProfessionFramework.addTrait("AllThumbs", {
    name = "UI_trait_AllThumbs",
    description = "UI_trait_AllThumbsDesc",
    cost = -4,
})

ProfessionFramework.addTrait("Asthmatic", {
    name = "UI_trait_Asthmatic",
    description = "UI_trait_AsthmaticDesc",
    cost = -5,
})

ProfessionFramework.addTrait("Claustophobic", {
    name = "UI_trait_claustro",
    description = "UI_trait_claustrodesc",
    cost = -4,
})

ProfessionFramework.addTrait("Clumsy", {
    name = "UI_trait_clumsy",
    description = "UI_trait_clumsydesc",
    cost = -2,
    exclude = { "Graceful", "Graceful2" },
})

ProfessionFramework.addTrait("Conspicuous", {
    name = "UI_trait_Conspicuous",
    description = "UI_trait_ConspicuousDesc",
    cost = -3,
    exclude = { "Inconspicuous" },
})

ProfessionFramework.addTrait("Cowardly", {
    name = "UI_trait_cowardly",
    description = "UI_trait_cowardlydesc",
    cost = -2,
})

ProfessionFramework.addTrait("Deaf", {
    name = "UI_trait_deaf",
    description = "UI_trait_deafdesc",
    cost = -12,
    exclude = { "HardOfHearing", "KeenHearing" }
})

ProfessionFramework.addTrait("Disorganized", {
    name = "UI_trait_Disorganized",
    description = "UI_trait_DisorganizedDesc",
    cost = -5,
})

ProfessionFramework.addTrait("Emaciated", {
    name = "UI_trait_emaciated",
    description = "UI_trait_emaciateddesc",
    cost = -8,
    xp = {
        [Perks.Strength] = -3
    },
    exclude = { "Underweight" },
})

ProfessionFramework.addTrait("Feeble", {
    name = "UI_trait_feeble",
    description = "UI_trait_feebledesc",
    cost = -6,
    xp = {
        [Perks.Strength] = -2
    },
})

ProfessionFramework.addTrait("HardOfHearing", {
    name = "UI_trait_hardhear",
    description = "UI_trait_hardheardesc",
    cost = -4,
    exclude = { "KeenHearing" }
})

ProfessionFramework.addTrait("HeartyAppitite", {
    name = "UI_trait_heartyappetite",
    description = "UI_trait_heartyappetitedesc",
    cost = -4,
    exclude = { "LightEater" },
})

ProfessionFramework.addTrait("Hemophobic", {
    name = "UI_trait_Hemophobic",
    description = "UI_trait_HemophobicDesc",
    cost = -5,
})

ProfessionFramework.addTrait("HighThirst", {
    name = "UI_trait_HighThirst",
    description = "UI_trait_HighThirstDesc",
    cost = -4,
})

ProfessionFramework.addTrait("Illiterate", {
    name = "UI_trait_Illiterate",
    description = "UI_trait_IlliterateDesc",
    cost = -12,
    exclude = { "SlowReader", "FastReader", "FastReader2" },
})

ProfessionFramework.addTrait("Out of Shape", {
    name = "UI_trait_outofshape",
    description = "UI_trait_outofshapedesc",
    cost = -6,
    xp = {
        [Perks.Fitness] = -2,
    }
})

ProfessionFramework.addTrait("Obese", {
    name = "UI_trait_obese",
    description = "UI_trait_obesedesc",
    cost = -6,
    xp = {
        [Perks.Fitness] = -2,
    },
    exclude = { "Underweight", "Very Underweight", "Emaciated" },
})

ProfessionFramework.addTrait("Overweight", {
    name = "UI_trait_overweight",
    description = "UI_trait_overweightdesc",
    cost = -4,
    xp = {
        [Perks.Fitness] = -1,
    },
    exclude = { "Obese", "Underweight", "Very Underweight", "Emaciated" },
})

ProfessionFramework.addTrait("Pacifist", {
    name = "UI_trait_Pacifist",
    description = "UI_trait_PacifistDesc",
    cost = -4,
})

ProfessionFramework.addTrait("ProneToIllness", {
    name = "UI_trait_pronetoillness",
    description = "UI_trait_pronetoillnessdesc",
    cost = -3,
})

ProfessionFramework.addTrait("ShortSighted", {
    name = "UI_trait_shortsigh",
    description = "UI_trait_shortsighdesc",
    cost = -1,
})

ProfessionFramework.addTrait("ShortSighted2", {
    name = "UI_trait_shortsigh",
    description = "UI_trait_shortsighdesc",
    profession = true,
    exclude = { "ShortSighted" }
})

ProfessionFramework.addTrait("Smoker", {
    name = "UI_trait_Smoker",
    description = "UI_trait_SmokerDesc",
    cost = -3,
})

ProfessionFramework.addTrait("SlowHealer", {
    name = "UI_trait_SlowHealer",
    description = "UI_trait_SlowHealerDesc",
    cost = -4,
})

ProfessionFramework.addTrait("SlowLearner", {
    name = "UI_trait_SlowLearner",
    description = "UI_trait_SlowLearnerDesc",
    cost = -8,
})

ProfessionFramework.addTrait("SlowReader", {
    name = "UI_trait_SlowReader",
    description = "UI_trait_SlowReaderDesc",
    cost = -2,
    exclude = { "FastReader", "FastReader2"},
})

ProfessionFramework.addTrait("SundayDriver", {
    name = "UI_trait_SundayDriver",
    description = "UI_trait_SundayDriverDesc",
    cost = -1,
    exclude = { "SpeedDemon", "SpeedDemon2"},
})

ProfessionFramework.addTrait("Thinskinned", {
    name = "UI_trait_ThinSkinned",
    description = "UI_trait_ThinSkinnedDesc",
    cost = -4,
})

ProfessionFramework.addTrait("Underweight", {
    name = "UI_trait_underweight",
    description = "UI_trait_underweightdesc",
    xp = {
        [Perks.Strength] = -1,
    },
    cost = -4,
})

ProfessionFramework.addTrait("Unfit", {
    name = "UI_trait_unfit",
    description = "UI_trait_unfitdesc",
    cost = -10,
    xp = {
        [Perks.Fitness] = -4,
    },
    exclude = { "Out of Shape" },
})

--[[
ProfessionFramework.addTrait("Unlucky", {
    name = "UI_trait_unlucky",
    description = "UI_trait_unluckydesc",
    cost = -4,
})
--]]

ProfessionFramework.addTrait("Very Underweight", {
    name = "UI_trait_veryunderweight",
    description = "UI_trait_veryunderweightdesc",
    cost = -6,
    xp = {
        [Perks.Strength] = -2,
    },
    exclude = { "Underweight", "Emaciated" },
})

ProfessionFramework.addTrait("Weak", {
    name = "UI_trait_weak",
    description = "UI_trait_weakdesc",
    cost = -10,
    xp = {
        [Perks.Strength] = -5
    },
    exclude = { "Strong", "Stout", "Feeble" },
})

ProfessionFramework.addTrait("WeakStomach", {
    name = "UI_trait_WeakStomach",
    description = "UI_trait_WeakStomachDesc",
    cost = -3,
    exclude = { "IronGut", "IronGut2"},

})