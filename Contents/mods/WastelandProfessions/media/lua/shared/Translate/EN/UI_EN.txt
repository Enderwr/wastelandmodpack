UI_EN = {
    UI_profdesc_unemployed = "One of many unable to find work in the country after the fuel crisis began.",
    UI_prof_survivalist = "Survivalist",
    UI_prof_riotpolice = "Riot Cop",
    UI_prof_convict = "Convict",
    UI_prof_Paramedic = "Paramedic",
    UI_prof_Mercenary = "Mercenary",
    UI_prof_peacekeeper = "Peacekeeper",
    UI_prof_tailor = "Tailor",
    UI_prof_Homeless = "Homeless",
    UI_prof_Racer = "Street Racer",
    UI_prof_Homemaker = "Homemaker",
    UI_prof_Librarian = "Librarian",

    UI_trait_small = "Small",
    UI_trait_smalldesc = "Lower physical strength but quick on your feet.",

    UI_trait_gunner = "Gun Enthusiast",
    UI_trait_gunnerdesc = "Experience with firearms and shooting.",

    UI_trait_explosives = "Explosives Expert",
    UI_trait_explosivesdesc = "Starts off knowing recipes for explosives and remote triggers.",

    UI_trait_yogi= "Yogi",
    UI_trait_yogidesc = "More flexible and mobile.",

    UI_trait_eagleeyeddesc = "Increased foraging radius by 0.5",
    UI_trait_shortsighdesc = "Reduced foraging radius by 2 unless wearing glasses",

    UI_Intro1 = "A DESPERATE FUEL SHORTAGE HAS LED TO WORLD-WIDE RECESSION",
    UI_Intro2 = "SCIENCE DEVELOPED A BACTERIUM TO ENHANCE BIOFUEL PRODUCTION",
    UI_Intro3 = "IT HAS MUTATED",

    UI_trait_dumpster = "Dumpster Diver",
    UI_trait_dumpsterDesc = "Increased foraging radius by 2, higher chance to find junk and garbage items, reduced effect from bad weather when in search mode.",

	UI_trait_WellRead = "Well Read",
	UI_trait_WellReadDesc = "Starts with knowledge of basic engine repair, generator connection and basic crafting for a few items, such as fishing rods and stick traps.",

	UI_trait_Motorhead = "Motorhead",
	UI_trait_MotorheadDesc = "Starts with knowledge of sports car engines, but not normal vehicles.",
}